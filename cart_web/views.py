from .models import Clients, Products, Order
from .serializers import ClientsSerializers, ProductsSerializers, OrderSerializers
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

# Create your views here.


class ClientsViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated, )
    queryset = Clients.objects.all()
    serializer_class = ClientsSerializers


class ProductsViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated, )
    queryset = Products.objects.all()
    serializer_class = ProductsSerializers


class OrdersViewSet(viewsets.ModelViewSet):

    permission_classes = (IsAuthenticated, )
    queryset = Order.objects.all()
    serializer_class = OrderSerializers
