from django.test import TestCase

from .models import Clients, Products

# Create your tests here.


class ClientsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Clients.objects.create(client_name='Thiago', email='thiago@gmail.com')

    def test_client_name_max_length(self):
        client = Clients.objects.get(id=1)
        max_length = client._meta.get_field('client_name').max_length
        self.assertEquals(max_length, 100)


class ProductsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Products.objects.create(product_name='Óleo', price=10.99)

    def test_product_name_max_length(self):
        product = Products.objects.get(id=1)
        max_length = product._meta.get_field('product_name').max_length
        self.assertEquals(max_length, 100)
