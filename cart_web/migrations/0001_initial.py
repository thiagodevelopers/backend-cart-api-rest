# Generated by Django 3.1.1 on 2020-09-25 03:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('client_name', models.CharField(max_length=100, verbose_name='Nome do Cliente')),
                ('email', models.EmailField(max_length=254, verbose_name='E-Mail')),
            ],
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('product_name', models.CharField(max_length=100, verbose_name='Nome do Produto')),
                ('price', models.FloatField(verbose_name='Preço')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('date_order', models.DateField(auto_now=True, verbose_name='Data Venda')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart_web.clients', verbose_name='Cliente')),
                ('products', models.ManyToManyField(to='cart_web.Products')),
            ],
        ),
    ]
