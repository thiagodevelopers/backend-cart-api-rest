from django.apps import AppConfig


class CartWebConfig(AppConfig):
    name = 'cart_web'
