from django.db import models
from django.urls import reverse

from django.db import models


class Clients(models.Model):
    id = models.AutoField(primary_key=True)
    client_name = models.CharField(
        max_length=100, blank=False, null=False, verbose_name="Nome do Cliente")
    email = models.EmailField(blank=False, null=False, verbose_name="E-Mail")

    def __str__(self):
        return self.client_name


class Products(models.Model):
    id = models.AutoField(primary_key=True)
    product_name = models.CharField(
        max_length=100, blank=False, null=False, verbose_name='Nome do Produto')
    price = models.FloatField(blank=False, null=False, verbose_name="Preço")

    def __str__(self):
        return self.product_name


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    date_order = models.DateField(auto_now=True, verbose_name="Data Venda")
    client = models.ForeignKey(
        Clients, on_delete=models.CASCADE, verbose_name="Cliente")
    products = models.ManyToManyField(Products)

    def __str__(self):
        return self.client.client_name
