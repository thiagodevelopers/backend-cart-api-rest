from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from .views import ClientsViewSet, \
    ProductsViewSet, \
    OrdersViewSet

router = routers.SimpleRouter()
router.register('v1/clients', ClientsViewSet, basename='clients')
router.register('v1/products', ProductsViewSet, basename='products')
router.register('v1/orders', OrdersViewSet, basename='orders')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
]
