from rest_framework import serializers
from .models import Clients, Products, Order

class ClientsSerializers(serializers.ModelSerializer):
    
    class Meta:
        id = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
        client_name = serializers.CharField()
        email = serializers.EmailField()

        model = Clients
        fields = '__all__'
        
        def create(self, validated_data):
            return Clients.objects.create(**validated_data)

        def update(self, validated_data):
            clients = Clients(**validated_data)
            clients.save()
            return clients

class ProductsSerializers(serializers.ModelSerializer):
    class Meta:
        id = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
        product_name = serializers.CharField()
        price = serializers.FloatField()

        model = Products
        fields = '__all__'

        def create(self, validated_data):
            return Products.objects.create(**validated_data)

        def update(self, validated_data):
            products = Products(**validated_data)
            products.save()
            return products

class OrderSerializers(serializers.ModelSerializer):
    class Meta:
        id = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
        date_order = serializers.DateField()
        client = serializers.PrimaryKeyRelatedField(many=True, read_only=True)        
        products = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

        model = Order
        fields = '__all__'

        def create(self, validated_data):
            return Order.objects.create(**validated_data)

        def update(self, validated_data):
            order = Order(**validated_data)
            order.save()
            return order