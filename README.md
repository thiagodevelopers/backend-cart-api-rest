# Market Cart - Back-end - API Rest

# 📖 Documentation

- [About](#-about)
- [Project status](#-project-status)
- [Technologies](#-technologies)
- [How to use](#-how-to-use)
- [Testing application](#-testing-application)
- [Host of API](#-host-of-api)
- [REST API](#-rest-api)
- [Features](#-features)

---

## 🔖 About

Software designed to manage transactions related to online store

---

## 📖 Project status

Django project development

---

## 🚀 Technologies

the project was developer with the technologies:

- [Ubuntu](https://ubuntu.com/)
- [Windows](https://www.microsoft.com/pt-br/windows/)
- [Python](https://www.python.org/)
- [Django](https://www.djangoproject.com/)
- [Docker](https://www.docker.com/)
- [AWS](https://aws.amazon.com/pt/)

---

## 🗂 How to use

## 🐳 Development enviroment

```
docker build -t backend-cart -f Dockerfile.dev .
```

```
docker run -d -p 5001:5001 backend-cart
```

Run in browser of your choice, for verify active service

```
http://localhost:5001/
```

# 📑 Note

-- Enviroment developer is created username "admin" and passsword "admin"

## 🐳 Production enviroment

```
sudo docker-compose up --build
```

```
sudo docker-compose -f docker-compose.yml exec app python manage.py createsuperuser
```

Inform your credentials

Run in browser of your choice, for verify active service

```
http://ec2-3-22-185-133.us-east-2.compute.amazonaws.com:8000/api/v1/clients/
```

---

## 🔨 Testing application

When being at the root of the project, if necessary, we can perform tests with the following command.

```
python manage.py test
```

---

# 🌍 Host of API

```
http://ec2-3-22-185-133.us-east-2.compute.amazonaws.com:8000/
```

---

# ➿ REST API

The REST API to the example app is described below.

## Get token of authentication

Exclusive router for login. Return token for validation in routers

### Request

`GET /api/token/`

    curl --header "Content-Type: application/json" --request POST --data "{\"username\": \"admin\", \"password\": \"admin\" }" http://localhost:8000/api/token/

### Response

    {"refresh":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYwMTMyNDk1OCwianRpIjoiYzI5NzJhMDJiMjgyNDExNWI2OWE3MTU1N2JlMDQyNzEiLCJ1c2VyX2lkIjoxfQ.xvYJ-RbITZiBUTaKKPjrAzvUlG0QpdsgRWbS7R7v51U","access":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjM4ODU4LCJqdGkiOiI1NDhiM2Y4M2IxYjk0M2FmYWI5NzczZWI0OTk1MmQ5NiIsInVzZXJfaWQiOjF9.l_aEk0QiOY9fOzyn_8Ji_zP1SwIB4QMfi3CT3IgIzqg"}

The routers of CRUD, is necessary send token of autentication in headers. Token type "Bearer"

## Get list of clients

### Request

`GET /api/v1/clients/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjM5Nzc5LCJqdGkiOiJjYmU4NTAwMDZmMTg0NjkzODdiMWIyYmVkZTNkODg3MSIsInVzZXJfaWQiOjF9.McY2lLiorIK3SVNtEGKN9rQFtOm_xP7EVVXVcXAR110" --request GET http://localhost:8000/api/v1/clients/

### Response

[{"id":4,"client_name":"Thiago de Souza Santos","email":"thiagosouzasantos@gmail.com"},{"id":5,"client_name":"Carmen Cecilia","email":"nunes@gmail.com"}]

## Create a new client

### Request

`POST /api/v1/clients/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjQwOTAxLCJqdGkiOiI4MjJkNDc5NDRmZmU0Yzc3OTE1MTA4ODAwNzQzOGJjMSIsInVzZXJfaWQiOjF9.KXZcekm0kLOQEN4nqsJQ-LMfF5PWbs6g_9BV4CiEiO8" --header "Content-Type: application/json" --request POST --data "{\"client_name\": \"Maria Jose\", \"email\": \"maria@gmail.com\" }" http://localhost:8000/api/v1/clients/

### Response

{"id":9,"client_name":"Maria Jose","email":"maria@gmail.com"}

## Get a specific client

### Request

`GET /api/v1/clients/id/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjQxODU0LCJqdGkiOiJkYjA0ZDcyZWFiMWU0NzBhYTA2NWRmYzVkODhhMWM0ZiIsInVzZXJfaWQiOjF9.DsXceg-MxMWAO0X7IcjHt_6VkDiif2qwFCMIJPZt758" --header "Content-Type: application/json" --request GET http://localhost:8000/api/v1/clients/9/

### Response

{"id":9,"client_name":"Maria Jose","email":"maria@gmail.com"}

## Get a non-existent client

### Request

`GET /api/v1/clients/id/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjQxODU0LCJqdGkiOiJkYjA0ZDcyZWFiMWU0NzBhYTA2NWRmYzVkODhhMWM0ZiIsInVzZXJfaWQiOjF9.DsXceg-MxMWAO0X7IcjHt_6VkDiif2qwFCMIJPZt758" --header "Content-Type: application/json" --request GET http://localhost:8000/api/v1/clients/20/

### Response

{"detail":"Not found."}

## Get changed client

### Request

`PUT /api/v1/clients/id/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjQzMzA3LCJqdGkiOiJmOTMxYzUxZWI1NjQ0NTZjYjJjZjU4MzNhYzQ1MzE2MyIsInVzZXJfaWQiOjF9.IJBCt0lpgvK2bI7XdKlG51D8jVONhT0ASzHZt6jQL-0" --header "Content-Type: application/json" --request PUT --data "{\"id\":\"10\", \"client_name\": \"Maria Jose dos Santos Lima\", \"email\": \"mariajosesantos@gmail.com\" }" http://localhost:8000/api/v1/clients/10/

### Response

{"id":10,"client_name":"Maria Jose dos Santos Lima","email":"mariajosesantos@gmail.com"}

## Delete a client

### Request

`DELETE /api/v1/clients/id/`

curl --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAxMjQyNjQ1LCJqdGkiOiJiOWEzMTJiN2I3YmE0YWVhOTM3MzFkM2NiZDY2NzY2OCIsInVzZXJfaWQiOjF9.vUEDhJuU7xNPgPJayS_nAb-mpkxAQSCAhzK49yNzANI" --header "Content-Type: application/json" --request DELETE http://localhost:8000/api/v1/clients/9/

### Response

[]

---

# 📰 Features

- [x] User register
- [x] Client register
- [x] Product register
- [x] Order register
- [ ] Documentation products
- [ ] Documentation orders
- [ ] Documentation user register
